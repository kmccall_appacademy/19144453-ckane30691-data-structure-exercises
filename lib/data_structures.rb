# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  sorted_arr = arr.sort
  (sorted_arr[0] - sorted_arr[sorted_arr.length - 1]).abs
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr.sort == arr ? true: false
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def is_vowel?(letter)
  vowels = ["a", "e", "i", "o", "u"]
  vowels.include?(letter) ? true: false
end

def num_vowels(str)
  count = 0
  str.split("").each do |letter| 
    count += 1 if is_vowel?(letter.downcase) 
  end
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  result = ""
  str.split("").each do |letter|
    result += letter unless is_vowel?(letter.downcase) 
  end
  result
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.split("").sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  used = []
  str.split("").each do |letter|
    if used.include?(letter.downcase)
      return true
    else
      used << letter.downcase
    end
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0]}#{arr[1]}#{arr[2]}) #{arr[3]}#{arr[4]}#{arr[5]}-#{arr[6]}#{arr[7]}#{arr[8]}#{arr[9]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  int_array = []
  str.split(",").each do |num| int_array << num.to_i end
  range(int_array)
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  result = arr.drop(offset % arr.length)
  temp_arr = arr.take(offset % arr.length)
  temp_arr.each do |item| result << item end
  result
end
